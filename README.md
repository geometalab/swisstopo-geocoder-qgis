# QGIS Plugin "Swiss GeoAdmin Bulk Geocoder"

Bulk geocoding of Swiss building addresses using the geocoding service of geo.admin.ch, the portal of swisstopo.

## Overview

The Swiss GeoAdmin Bulk Geocoder provides geocoding functionality for building addresses in Switzerland using the geo.admin.ch service, the portal hosted by swisstopo. The plugin allows users to perform bulk geocoding on a vector layer by adding coordinates to address attributes.

## Features

- Geocode addresses from a vector layer.
- Create a new vector layer with geocoded points.

## Requirements

- QGIS 3.x
- Internet connection to access the SwissTopo geocoding service.

## Installation

1. Open QGIS.
2. Navigate to **Plugins > Manage and Install Plugins...**
3. In the **Plugin Manager**, search for "swisstopo geocoder" and click **Install**.

## Usage

1. Open QGIS.
2. Load a vector layer with address attributes.
3. Run the "swisstopo geocoder" plugin from the Processing Toolbox.
4. Select the input layer and the address attribute.
5. Click **Run** to perform geocoding.

## Development

If you want to contribute to the development of the swisstopo geocoder plugin, follow these steps:

1. Clone the repository:

   git clone <<repo>>.git

2. Write me an e-mail so I can review it. / create an issue.
3. This an open source project so u can also publish ur own modified version if I don't answer or react to your changes.

## License

This project is licensed under the GNU General Public License (GPL) 3


